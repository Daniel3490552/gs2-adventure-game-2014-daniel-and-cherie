using UnityEngine;
using System.Collections;

public static class Tags
{
	public static readonly string Name = "#Name";
	
	
	/// <summary>
	/// Returns a string with converted tags, using a particular game object.
	/// </summary>
	public static string ConvertTagsForObject(GameObject obj, string s)
	{
		string converted = s;
		
		if (s.Contains(Tags.Name))
			converted = converted.Replace(Tags.Name, obj.name);
		
		return converted;
	}
}