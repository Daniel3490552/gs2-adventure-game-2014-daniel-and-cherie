using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour
{
	void Update()
	{
		// only interested in main button press
		if (!Input.GetButtonDown(Game.Instance.MainButtonName))
			return;
		
		// let an interactable's scene menu handle the press
		if (Player.Instance.InInteractableSceneMenu)
			return;
		
		// let an interactable's inventory menu handle the press
		if (Player.Instance.Inventory.InInteractableMenu)
			return;
		
		// let inventory handle the press
		if (Player.Instance.Inventory.IsScreenPointInside(AdventureUtility.InvertedMouseY))
			return;
		
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
        if (Physics.Raycast(ray, out hit))
			OnMainButtonPress(hit.collider.gameObject, hit.point);
	}
	
	/// <summary>
	/// What to do when player presses main button.
	/// </summary>
	/// <param name="target">
	/// Object player pressed on, null if pressed on empty space.
	/// </param>
	/// <param name="point">
	/// Point player pressed on.
	/// </param>
	protected virtual void OnMainButtonPress(GameObject target, Vector3 point)
	{
		// let player object handle the press
		if (target.GetComponent(typeof(Player)))
			return;
		
		// let pressed interactable handle the press
		Interactable i = (Interactable)target.GetComponent(typeof(Interactable));
		if ((i != null) && i.IsScreenPointInside(AdventureUtility.InvertedMouseY))
			return;
		
		// don't move if clicked on nothing while player is in middle of a dual interaction
		if (Player.Instance.InDualInteraction)
			return;
		
		// else just move player to point
		Player.Instance.Movement.Move(point);
	}
}
